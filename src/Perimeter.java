// @author Andrew Baumher

//Test imports for redirecting input from system.in to a file.
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.util.Scanner;

//Storage Class for each rectangle. Not needed, par se, but convienent.
class Rectangle
{
	private final int left, right, top;

	public Rectangle(int left, int right, int top)
	{
		this.left = left;
		this.right = right;
		this.top = top;
	}

	//getters. private data not required, but habit, i guess.
	public int getHeight()
	{
		return top;
	}
	public int getXMax()
	{
		return right;
	}
	public int getXMin()
	{
		return left;
	}
	public int getLength()
	{
		return right - left;
	}

	public boolean equals(Rectangle rect)
	{
		return top == rect.top;
	}
	public int compare(Rectangle rect)
	{
		//if returns positive, goes before the current elem. i do this b/c the sort is backwards

		//if y is equal, sort by x asc.
		if (this.equals(rect))
		{
			return rect.left - this.left;
		}
		//otherwise sort y desc.
		else
		{
			return this.top - rect.top;
		}
	}
}

//Link in the list structure i create. stores an xmax and xmin.
class Link
{
	private int xmin, xmax;
	public Link next;

	public Link(int min, int max)
	{
		xmin = min;
		xmax = max;
	}
	//setters/getters for private data.
	public void setXMin(int min)
	{
		xmin = min;
	}
	public void setXMax(int max)
	{
		xmax = max;
	}
	public int getXMin()
	{
		return xmin;
	}
	public int getXMax()
	{
		return xmax;
	}

}

//list structure. uses links above. implemented as a singleton.
class listStruct
{

	private static listStruct instance = null;
	public Link first;
	public Link current;
	public Link previous;
	public int count;
	public boolean isEmpty()
	{
		return first == null;
	}
	private listStruct()
	{
		first = null;
		current = first;
		previous = null;
		count = 0;
	}

	//initializes the singleton. lazy use, only defines if/when first used
	public static listStruct getListStruct()
	{
		if (instance == null)
		{
			instance = new listStruct();
		}
		return instance;
	}

	//adds link before current. list is sorted, so sometimes it needs to go before current link
	public void addLinkBeforeCurrent(Link newLink)
	{
		count++;
		if (isEmpty())
		{
			first = newLink;
			current = first;
			previous = null;
		}
		if (previous == null)
		{
			newLink.next = first;
			first = newLink;
			current = first;
		}
		else
		{
			newLink.next = current;
			previous.next = newLink;
			current = previous.next;
		}
	}
	//same as above, but after this time.
	public void addLinkAfterCurrent(Link newLink)
	{
		count++;
		if (isEmpty())
		{
			first = newLink;
			current = first;
			previous = null;
		}
		else if (current == null)
		{
			current = newLink;
			previous.next = current;
		}
		else if (current.next == null)
		{
			current.next = newLink;
			previous = current;
			current = current.next;
		}
		else
		{
			Link temp = current.next;
			current.next = newLink;
			newLink.next = temp;
			previous = current;
			current = current.next;
		}
	}

	//same as above addlinkbefore, but different parameters.
	public void addLinkBeforeCurrent(int xmin, int xmax)
	{
		addLinkBeforeCurrent(new Link(xmin, xmax));
	}
	//same as above addlinkafter, but different parameters.
	public void addLinkAfterCurrent(int xmin, int xmax)
	{
		addLinkAfterCurrent(new Link(xmin, xmax));
	}

	//removes current link, updates all related pointers
	public void removeLink()
	{
		if (count == 1)
		{
			first = null;
			previous = null;
			current = first;
			count--;
		}
		else if (!isEmpty())
		{
			if (first == current)
			{
				current = first.next;
				first = current;
			}
			else
			{
				previous.next = current.next;
				current = previous.next;
			}
			count--;
		}
	}

	//steps through list one link.
	public void stepForward()
	{
		previous = current;
		current = current.next;

	}
}

//houses main function and all perimeter calculations. also sorts list.
public class Perimeter
{
	//sum is calculated as a sweep line. when the line hits a new rectangle, this
	//functions checks against the list of stored xmin and xmax values to see if this
	//new rectangle's xmin/xmax overlap any of the stored values. if no overlap occurs
	// it is added to the list. if it is enveloped by an element of the list, it is 
	//ignored. otherwise, any and all elements of the list that this xmax and xmin operlap
	//are removed/or and combined into one new element. 
	static int overlap(int total, int xmin, int xmax)
	{
		listStruct partialColumns = listStruct.getListStruct();

		//checks if new xmin is greater than the xmax of the current elem of the list
		//if so, step to the next element, until end of list reached or xmin 
		//is not greater than the xmax of the current
		while (partialColumns.current.next != null && xmin > partialColumns.current.getXMax())
		{
			partialColumns.stepForward();
		}
		//if null and still greater than the max, add it as a new element on the right
		if (partialColumns.current.next == null && xmin > partialColumns.current.getXMax())
		{
			partialColumns.addLinkAfterCurrent(xmin, xmax);
			total += (xmax - xmin) * 2;
			return total;
		}

		//otherwise, if it's xmax is less than the xmin of the current, add as a new elem on left
		else if (xmax < partialColumns.current.getXMin())
		{ 
			partialColumns.addLinkBeforeCurrent(xmin, xmax);
			total += (xmax - xmin) * 2;
			return total;
		}

		//if it is enveloped by current elem, ignore it.
		else if (xmin >= partialColumns.current.getXMin()
				&& xmax <= partialColumns.current.getXMax())
		{
			return total;
		}
		
		//if it overlaps and continues on to the left
		if (xmin < partialColumns.current.getXMin())
		{
			//total increases by amount not overlapped on the left * 2
			total += (partialColumns.current.getXMin() - xmin) * 2;
			//update the min at the current elem to new min.
			partialColumns.current.setXMin(xmin);
			if (xmax <= partialColumns.current.getXMax())
			{
				return total;
			}
		}
		//if it overlaps and continues on to the right
		if (xmax > partialColumns.current.getXMax())
		{
			//fun stuff occurs. aka this part was a pain and the most error prone.
			
			//tempLeft is used to calculate the amount not overlapped. 
			int tempLeft = partialColumns.current.getXMax();
			//I delete the current overlapped entry. this keeps its min value
			//which is then used to create a new element. 
			int newXMin = partialColumns.current.getXMin();

			partialColumns.removeLink();

			//consume any elements to the right that are enveloped by this new range
			while (partialColumns.current != null && xmax > partialColumns.current.getXMax())
			{
				//if consumed, update templeft and add to total nonconsumed bits, dummy.
				//Note to grader: that error gave me fits, so a bit of self insulting was in order
				total += (partialColumns.current.getXMin() - tempLeft) * 2;
				tempLeft = partialColumns.current.getXMax();
					//error fixed. 

				partialColumns.removeLink();
			}
			//if we consume every element in the list after the starting one
			if (partialColumns.isEmpty() || partialColumns.current == null)
			{
				partialColumns.addLinkAfterCurrent(newXMin, xmax);
				total += (xmax - tempLeft) * 2;
			}

			//if we have found an element we don't overlap at all.
			else if (partialColumns.current.getXMin() > xmax)
			{
				//add the new element from newXMin to xmax
				partialColumns.addLinkBeforeCurrent(newXMin, xmax);
				total += (xmax - tempLeft) * 2;
			}
			//otherwise, we overlap this element. we need to combine these two.
			else
			{
				total += (partialColumns.current.getXMin() - tempLeft) * 2;
				partialColumns.current.setXMin(newXMin);
			}
		}
		//return the total
		return total;
	}

	//calculates the sum. for each rectangle, calls overlap.
	static int calculateSum(Rectangle[] theArr)
	{
		int total = 0, rectArrLoc = 0;
		listStruct partialColumns = listStruct.getListStruct();
		Rectangle currentRect = theArr[rectArrLoc++];
		
		//adds the initial rectangle to the list. and adds initial length to total
		partialColumns.addLinkAfterCurrent(currentRect.getXMin(), currentRect.getXMax());
		//Note: for all values where total is changed, it is *2, as they have a 
		// corresponding top/bottom or left/right. 
		total += currentRect.getLength() * 2;

		//recArrLoc is our counter, does not need to be reinitialized.
		for (; rectArrLoc < theArr.length; rectArrLoc++)
		{
			//if height changes
			int temp = currentRect.getHeight() - theArr[rectArrLoc].getHeight();
			if (temp != 0)
			{
				//move current back to first.
				partialColumns.current = partialColumns.first;
				partialColumns.previous = null;
				//add the change in height to total. note that if we have multiple elements
				//in the list, they all change height, so they all need to be counted.
				total += temp * 2 * partialColumns.count;
			}
			//check current rectangle for any overlap, update total.
			total = overlap(total,
					theArr[rectArrLoc].getXMin(), theArr[rectArrLoc].getXMax());
			//update the current rectangle
			currentRect = theArr[rectArrLoc];
		}
		//add the final height values in.
		total += currentRect.getHeight() * 2;
		return total;
	}

	//splits the list recursively. when the lists are split, calls the sort method and returns result.
	static Rectangle[] splitList(Rectangle[] theArr)
	{
		if (theArr.length == 1)
		{
			return theArr;
		}
		Rectangle[] leftArr = new Rectangle[theArr.length / 2];
		Rectangle[] rightArr = new Rectangle[theArr.length - theArr.length / 2];
		System.arraycopy(theArr, 0, leftArr, 0, theArr.length / 2);
		System.arraycopy(theArr, theArr.length / 2, rightArr, 0, theArr.length - theArr.length / 2);
		leftArr = splitList(leftArr);
		rightArr = splitList(rightArr);
		return sortMe(leftArr, rightArr);

	}
	//sort method. uses a merge sort. if finds redundant rectangle, ignores it.
	static Rectangle[] sortMe(Rectangle[] leftArr, Rectangle[] rightArr)
	{
		Rectangle[] sortedList = new Rectangle[leftArr.length + rightArr.length];
		int x = 0, y = 0, length = leftArr.length + rightArr.length;
		for (int z = 0; z < sortedList.length; z++)
		{
			//if we have any elements to sort
			if (x < leftArr.length && y < rightArr.length)
			{
				//compare the two. if > 0, the left element goes first.
				int temp = leftArr[x].compare(rightArr[y]);
				if (temp > 0)
				{
					sortedList[z] = leftArr[x++];
				}
				//otherwise if not equal, right goes first.
				else if (temp < 0)
				{
					sortedList[z] = rightArr[y++];
				}
				//if equal in y direction and xmin
				else
				{
					//if left element is longer than right, ignore right, add left
					if (leftArr[x].getLength() > rightArr[y].getLength())
					{
						sortedList[z] = leftArr[x++];
						y++;
					}
					//otherwise add right, ignore left
					else
					{
						sortedList[z] = rightArr[y++];
						x++;
					}
					//decrease length by one. useful to know if we omitted redundant rectangles
					length--;
				}
			}
			//if we are out of left elements.
			else if (x >= leftArr.length)
			{
				//and we arent out of right elements.
				if (y < rightArr.length)
				{
					//add a right element
					sortedList[z] = rightArr[y++];
				}
			}
			//if we are out of right elements
			else if (y >= rightArr.length)
			{
				//and we arent out of left elements
				if (x < leftArr.length)
				{
					sortedList[z] = leftArr[x++];
				}
			}
		}
		//a duplicate was hit and removed. only occurs when top and left are the same
		//the one with the shorter length is removed.
		if (length != leftArr.length + rightArr.length)
		{
			//shorten the list (nulls make life annoying).
			Rectangle[] shortenedList = new Rectangle[length];
			System.arraycopy(sortedList, 0, shortenedList, 0, length);
			return shortenedList;
		}
		return sortedList;
	}

	//END SORT
	//STAR MAIN
	
	//runs the program. Sorts the list, calculates the sum, and prints it.
	public static void main(String[] Args) //throws FileNotFoundException
	{
		//tests program against a file. 
		//System.setIn(new FileInputStream(new File("testInput.txt")));

		Scanner input = new Scanner(System.in);
		int nRectangles = input.nextInt();
		Rectangle[] rectList = new Rectangle[nRectangles];
		for (int x = 0; x < nRectangles; x++)
		{
			rectList[x] = new Rectangle(input.nextInt(), input.nextInt(), input.nextInt());
		}
		//the sort is O(n log(n). since both parts are done separately, algorithm remains
		//O(n log(n) + O(n log(n), or O(n log n)
		rectList = splitList(rectList);

		int perimeter = calculateSum(rectList);

		System.out.println(perimeter);
	}
}

/*Answers To Questions:
 Note: These are put in a questions.doc file for submission on mycourses, but try does not allow this.
 Problem 1:
 A. This algorithm recursively checks every number in the array. when two are equal
 it stops and returns. Each Recursive call is nlog(n), but it is called twice.
 as of now the time constraints are 2nlog(n). Additionally, each call does another 2 steps
 and there are nlogn calls. so we get 2nlog(n) + 2nlog(n) = 4nlog(n). However, in
 terms of time complexity, this is simply considered nlog(n) time.

 B. Worst Case is that n!= 1. the algoirthm checks every element of the array from 1 to n.
 Therefore, if you have to check more than one element
 Best Case is n=1. in that case, T(1) = C. For all other n, Best Case = Worst Case
 Worst/Average Case: T(k) = 2T(k/2) + Dk + C'
 C' is the additional constant number of steps taken when not recursing
 D is the number of steps taken per number while recursting. note that D is variable
 and changes depending on what happens during the recursion. That said, it's value is
 not dependant on times run.
 T(n) = 2T(n/2) + Dn + C' =
 = 2(2T(n/4) + D*n/2 + C') + Dn + C'
 = 4T(n/4) + Dn + 2C' + Dn + C'
 = 4T(n/4) + 2Dn + 3C'
 = 4(2T(n/8) + D*n/4 +C') + 2Dn + 3C'
 = 8T(n/8) + 3Dn + 7C' etc.
 This can be summarized as:
 T(n) = 2^k*T(n*2^-k) + kDn + Summation(1 to k of 2^k-1)C'
 T(n) = 2^k*T(n*2^-k) + kDn + (2^k - 1)C
 let n be a power of 2, k = log n.
 T(n) = 2^log(n)*T(n*2^-log(n)) + log(n)Dn + (2^log(n) -1) C'
 T(n) = n*T(n/n) + Dnlog(n) + (n-1)C'
 T(n) = Cn + Dnlogn + nC' -C'
 T(n) = Dnlogn + n(C+C') - C'
 If we remove all constants, which are not relevant to this, we find the tight bound
 is nlog(n).

 C. This algorithm finds 4 things: a summ of all positive numbers e.g the maximum sum
 that can be obtained, the sum of all numbers from the left bound to the right pivot point
 the sum of all numbers from the right to the left pivot point, and the overall sum.
 these pivot points are relative max's on the respetive sides.

 3:
 1. a= 1, b = 3. f(n)= 1 I determined that the second case works. 1 = n^0, and log base 3 of 1 = 0.
 f(n) = n^0 * log^0(n). log^0 of n = 1. n^0 = 1. f(n) = 1*1. f(n) = 1, so this holds.
 ergo, T(n) = theta (log(n)).

 2. a = 2, b = 4. f(n) = n. case 1 does not work, as c has to be > 0. log base b of a - c must equal 1
 for this to hold. log base b of a < 1. ergo, c must be negative for this to hold, but a cannot be 
 less than zero. the second case does not hold, as log base b of a != 1. the third case
 holds, however, as f(n) = n^1. 1 > log base b of a.
 ergo, T(n) = theta (n).

 3. a = 4, b = 2. f(n) = n log(n). case 1 works, as log base 4 of 2 = 2. n^2 > n log (n).
 Thus, there exists a c > 0 where n ^ (2 - c) = n log n.
 ergo, T(n) = theta n^2


 */
